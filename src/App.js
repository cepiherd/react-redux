import React,{Component} from 'react';
import Header from './components/header';
import NewsList from './components/news_list';
import JSON from '../src/db.json';


class App extends Component{
  constructor(){
    super();
    this.state = { 
      news : JSON 
    }
  }
  render(){
    console.log(this.state.news)
    return(
      <>
        <Header/>
        <NewsList news={this.state.news}/>
      </>
    )
  }
}


export default App;