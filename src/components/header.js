import React from 'react'
import { EventEmitter } from 'events';


// adding styles 
// let styles = {
//     header: {
//         background : '#03a9f4'
//     },
//     logo: {
//         color: '#fff',
//         fontFamily:'Roboto',
//         textAlign: 'center'
//     }
// }

class Header extends React.Component{
    // ketika menggunaka type component class maka di perlukan render untuk 
    // memanggil return
   

    // pelajaran state 

    state  = { 
        // title:'The keywords are : ',
        keyword : '',
        // count  :0
    }

    // make function 

    hello = () => {
        console.log('My hero')
    }

    inputChange = (event) => {
        this.setState({
            keywords: event.target.value
        })
        // console.log(event.target.value)
    }

    // addOne() {
    //     this.setState({
    //         count : this.state.count +1
    //     })
       
    // }
    render(){
        return (
            <header >
                <div 
                onClick={this.hello}>
                        LOGO
                </div>
                <input onChange={this.inputChange}/>


                {/* <div>{this.state.title}</div>
                <div>{this.state.keywords}</div>
                <br/>
                
                <div>{this.state.count}</div>
                <button onClick={()=> this.addOne() }>Add one</button> */}
            </header>
           
        )
    }
}


export default Header;